namespace MyFirstStartUP
{
    public interface IDo<out TOut>
    {
        TOut Method();
    }
}