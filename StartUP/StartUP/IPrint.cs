namespace MyFirstStartUP
{
    public interface IPrint<in TIn>
    {
        void Print(TIn input);
    }
}